package qa.automated.web.bci.Definitions;

import static org.junit.Assert.assertTrue;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import qa.automated.web.bci.Launcher.ApplicationLauncherAndroid;

public class BCI_MobileLogin {

	@When("aparece boton para ingresar")
	public void aparece_boton_para_ingresar() throws Throwable {
		// validacion abre apk
		String mensaje = ApplicationLauncherAndroid.pageVisitasLogin.textoSucursales();
		boolean iguales = mensaje.equalsIgnoreCase(ApplicationLauncherAndroid.properties.SALUDOSUCURSAL);
		assertTrue(iguales);
	}

	@Then("hago click al boton ingresar")
	public void hago_click_al_boton_ingresar() throws Throwable {
		ApplicationLauncherAndroid.pageVisitasLogin.msgCon();
		Thread.sleep(2000);
	}

	@When("ingresa rut {string}..")
	public void ingresa_rut(String string) throws Throwable {
		String mensaje = ApplicationLauncherAndroid.pageVisitasLogin.textoProblemas();
		boolean iguales = mensaje.equalsIgnoreCase(ApplicationLauncherAndroid.properties.SALUDOLOGIN);
		assertTrue(iguales);
		// validacion
		ApplicationLauncherAndroid.pageVisitasLogin.clickCampoRut();
		ApplicationLauncherAndroid.pageVisitasLogin.ingresarRut(string);
		Thread.sleep(2000);
	}

	@Then("valido rut ingresado con puntos y guiones..")
	public void valido_rut_ingresado_con_puntos_y_guiones() throws Throwable {
		String rut = ApplicationLauncherAndroid.pageVisitasLogin.obtenerRut();
		boolean resultado = ApplicationLauncherAndroid.pageVisitasLogin.validarRut(rut);
		assertTrue(resultado);
	}

	@When("Ingresar letras campo rut {string}..")
	public void ingresar_letras_campo_rut(String string) throws Throwable {
		// ApplicationLauncherAndroid.pageVisitasLogin.ingresarRut(string);
	}

	@Then("Comprobar que no ingresa letras..")
	public void comprobar_que_no_ingresa_letras() throws Throwable {
		// String rut = ApplicationLauncherAndroid.pageVisitasLogin.obtenerRut();
		// Thread.sleep(2000);
		// assertTrue("Rut no permite letras correcto", rut.equalsIgnoreCase("") ||
		// UsoCom.isNumeric(rut));
	}

	@When("Ingresar letra k campo rut {string}..")
	public void ingresar_letra_k_campo_rut(String string) throws Throwable {
		ApplicationLauncherAndroid.pageVisitasLogin.ingresarRut(string);
	}

	@Then("Comprobar que ingresa letra correctamente..")
	public void comprobar_que_ingresa_letra_correctamente() throws Throwable {
		String rut = ApplicationLauncherAndroid.pageVisitasLogin.obtenerRut();// ****************
		Thread.sleep(2000);
		assertTrue("Rut permite letra K correcto", rut.equals("k") || rut.equals("K"));

	}

	@When("ingreso en campo clave {string} con cinco digitos..")
	public void ingreso_en_campo_clave_con_cinco_digitos(String string) throws Throwable {
		ApplicationLauncherAndroid.pageVisitasLogin.ingresarClave(string);
	}

	@Then("Valido ingreso de clave con largo de clave invalida..")
	public void valido_ingreso_de_clave_con_largo_de_clave_invalida() throws Throwable {
	}

	@When("ingreso rut {string} y clave {string}")
	public void ingreso_rut_y_clave(String string, String string2) throws Throwable {
		ApplicationLauncherAndroid.pageVisitasLogin.ingresarRut(string);
		ApplicationLauncherAndroid.pageVisitasLogin.ingresarClave(string2);

	}

	@Then("Se valida ingreso de clave y rut validos de forma exitosa")
	public void se_valida_ingreso_de_clave_y_rut_validos_de_forma_exitosa() throws Throwable {
		ApplicationLauncherAndroid.pageVisitasLogin.btnLoginActivo();
		Thread.sleep(5000);
		ApplicationLauncherAndroid.pageVisitasLogin.ClickBtnVer();
		Thread.sleep(2000);
		ApplicationLauncherAndroid.pageVisitasLogin.ingresar();
		Thread.sleep(6000);
	}

	@When("aparece pantalla de terminos")
	public void aparece_pantalla_de_terminos() throws Throwable {
		Thread.sleep(6000);
		String mensaje = ApplicationLauncherAndroid.pageVisitasLogin.textoCondiciones();// ***
		boolean iguales = mensaje.equalsIgnoreCase(ApplicationLauncherAndroid.properties.SALUDOCONDICIONES);
		assertTrue(iguales);
	}

	@Then("acepto terminos y hago click al boton aceptar")
	public void acepto_terminos_y_hago_click_al_boton_aceptar() throws Throwable {
		ApplicationLauncherAndroid.pageVisitasLogin.clickChkTerminos();
		Thread.sleep(2000);
		ApplicationLauncherAndroid.pageVisitasLogin.btnTerminosActivo();
		Thread.sleep(2000);
		ApplicationLauncherAndroid.pageVisitasLogin.clickBtnAceptarTerminos();// **
	}

	@When("aparece mensaje de version de prueba")
	public void aparece_mensaje_de_version_de_prueba() throws Throwable {
		Thread.sleep(6000);
		String mensaje = ApplicationLauncherAndroid.pageVisitasLogin.textoComenzarVersionPrueba();
		boolean iguales = mensaje.equalsIgnoreCase(ApplicationLauncherAndroid.properties.SALUDOVERSIONPRUEBA);
		assertTrue(iguales);
	}

	@Then("hago click en comenzar")
	public void hago_click_en_comenzar() throws Throwable {
		Thread.sleep(2000);
		ApplicationLauncherAndroid.pageVisitasLogin.clickBtnVersionPrueba();
		Thread.sleep(8000);
	}
}